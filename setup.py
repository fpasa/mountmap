from setuptools import setup, find_packages

setup(
    name="mountmap",
    version="1.0.0",
    author="Francesco Pasa",
    author_email="francescopasa@gmail.com",
    packages=find_packages(),
    install_requires=[
        "numpy >= 1.11.1",
        "imageio >= 2.6.1",
        "scikit-image >= 0.15.0",
        "aggdraw >= 1.3.11",
        "pillow >= 6.2.0",
        "scipy >= 1.3.1",
        "overpy >= 0.4",
        "PyYAML >= 5.1.2",
        "shapely >= 1.6.4",
        "python-fontconfig >= 0.5.1",
        "shapely >= 1.6.4",
        "pyeasyga >= 0.3.1",
        "tifffile >= 2019.7.26.2",
    ],
)
