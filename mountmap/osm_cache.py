import os
import pathlib


class OSMCache:
    def __init__(self, cache_folder=None):
        if cache_folder is None:
            cache_folder = os.path.expanduser("~/.cache/mountmap/osm")

        self.cache_folder = pathlib.Path(cache_folder)
        os.makedirs(self.cache_folder, exist_ok=True)

    def get(self, filename, file_load_func):
        cache_path = self.cache_folder / filename

        if not os.path.exists(cache_path):
            osm_data = file_load_func()

            with open(cache_path, "w") as osm_file:
                osm_file.write(osm_data)

            return osm_data

        with open(cache_path) as osm_file:
            return osm_file.read()
