from .features import Features


class Water(Features):
    def _get_features(self):
        relations = self.osm.query("relations", ["water", "waterway"])
        ways = self.osm.query("ways", ["water", "waterway"])

        natural_relations = self.osm.query("relations", {"natural": "water"})
        natural_ways = self.osm.query("ways", {"natural": "water"})

        return [
            *relations,
            *ways,
            *natural_relations,
            *natural_ways,
        ]

    def _get_feature_type(self, tags):
        waterway = tags.get("waterway")
        water = tags.get("water")
        natural = tags.get("natural")

        return waterway or water or natural
