from ..render_plugin import RenderLevel, RenderPlugin


class Tracks(RenderPlugin):
    level = RenderLevel.FEATURES

    @property
    def tracks(self):
        return self.config or []

    def render_artist(self, artist):
        for track in self.tracks:
            self._render_track(artist, track)

    def _render_track(self, artist, track):
        points = self._get_track_points(track)

        if points:
            stroke_color = track.get("stroke_color", "#e74c3c")
            stroke_width = track.get("stroke_width", 3)

            artist.set_pen(stroke_color, stroke_width)
            artist.draw_path([points])

    def _get_track_points(self, track):
        points = track.get("points", [])
        lon_lat_points = map(lambda p: tuple(p[::-1]), points)
        pixel_points = self.converter.convert(lon_lat_points)
        return pixel_points
