import shapely.geometry
import numpy as np

from ..render_plugin import RenderLevel, RenderPlugin

NATURAL_GROUND_TYPES = [
    "wood",
    "grassland",
    "heath",
    "scrub",
    "moor",
    "mud",
    "wetland",
    "bare_rock",
    "scree",
    "shingle",
    "sand",
    "beach",
    "dune",
    "glacier",
    "coastline",
]


class LandCover(RenderPlugin):
    level = RenderLevel.LAND_AREAS

    @property
    def precision(self):
        return self.config.get("precision", 0)

    def render_artist(self, artist):
        features = self._get_land_features()

        for feature in features:
            self._render_feature(artist, feature)

    def _get_land_features(self):
        relations = self.osm.query("relations", ["landuse", "natural"])
        ways = self.osm.query("ways", ["landuse", "natural"])

        # Ways contain coastlines and therefore must be rendered first
        features = [*self._norm_ways(ways), *self._norm_relations(relations)]

        return self._filter_land_features(features)

    def _norm_relations(self, relations):
        return [self._norm_relation(relation) for relation in relations]

    def _norm_relation(self, relation):
        roles, normed_ways = list(
            zip(*[(way["role"], self._norm_way(way)) for way in relation["ways"]])
        )

        if not normed_ways:
            return relation["tags"], []

        joined_ways, joined_roles = self._join_ways(normed_ways, roles)
        if not joined_ways:
            return (
                relation["tags"],
                [
                    self._ensure_counterclockwise(points)
                    if role == "inner"
                    else self._ensure_clockwise(points)
                    for role, (_, points) in zip(roles, normed_ways)
                ],
            )

        return (
            relation["tags"],
            [
                self._ensure_counterclockwise(points)
                if role == "inner"
                else self._ensure_clockwise(points)
                for role, (_, points) in zip(joined_roles, joined_ways)
            ],
        )

    def _ensure_counterclockwise(self, points):
        return points[::-1] if self._clockwise(points) else points

    def _ensure_clockwise(self, points):
        return points if self._clockwise(points) else points[::-1]

    @staticmethod
    def _clockwise(points):
        direction = 0
        for (x1, y1), (x2, y2) in zip(points[:-1], points[1:]):
            direction += (x2 - x1) * (y2 + y1)
        return direction >= 0

    def _norm_ways(self, ways):
        normed_ways = [self._norm_way(way) for way in ways]
        normed_ways = self._join_coastlines(normed_ways)
        return [(tags, [points]) for tags, points in normed_ways]

    def _norm_way(self, way):
        return way["tags"], self._way_to_points(way)

    def _way_to_points(self, way):
        points = self.converter.convert(way["nodes"])

        if self.precision:
            points = self._simplify_polygon(points)

        return points

    def _simplify_polygon(self, points):
        if len(points) < 3:
            return points

        polygon = shapely.geometry.Polygon(points)
        simplified_polygon = polygon.simplify(self.precision)
        return simplified_polygon.exterior.coords

    def _join_coastlines(self, ways):
        coastlines = []
        rest = []
        for way in ways:
            if way[0].get("natural") == "coastline":
                coastlines.append(way)
            else:
                rest.append(way)

        return [
            # Put coastlines first, they must be rendered before the rest
            *self._join_ways(coastlines)[0],
            *rest,
        ]

    def _join_ways(self, ways, roles=None):
        # Stitch ways together in the right order
        ways, group_roles = self._group_and_sort_ways(ways, roles)

        grouped_ways = []
        for group in ways:
            tags = {}
            points = []
            for way_tags, way_points in group:
                tags.update(way_tags)
                points.extend(way_points)

            grouped_ways.append((tags, points))

        return grouped_ways, group_roles

    @staticmethod
    def _group_and_sort_ways(ways, roles=None):
        # TODO: this function sucks

        remaining_ways = list(ways)
        remaining_roles = list(roles) if roles else [()] * len(remaining_ways)

        if not remaining_ways:
            return ways, roles

        sorted_ways = []
        sorted_roles = []
        current_group = [remaining_ways.pop()]
        current_role = remaining_roles.pop()

        while remaining_ways:
            _, last_way_points = current_group[-1]
            last_point = np.array(last_way_points[-1])

            found = False
            for i, ((tags, points), role) in enumerate(
                zip(remaining_ways, remaining_roles)
            ):
                if role != current_role:
                    continue

                if not points:
                    found = True
                    continue

                if (points[0] == last_point).all():
                    current_group.append((tags, points))
                    found = True
                    break

                if (points[-1] == last_point).all():
                    current_group.append((tags, list(reversed(points))))
                    found = True
                    break

            if found:
                del remaining_ways[i]
                del remaining_roles[i]

            if not found or not remaining_ways:
                sorted_ways.append(current_group)
                sorted_roles.append(current_role)
                if remaining_ways:
                    current_group = [remaining_ways.pop()]
                    current_role = remaining_roles.pop()

        return sorted_ways, sorted_roles

    def _filter_land_features(self, features):
        return [
            feature for feature in features if self._is_land_cover_feature(feature[0])
        ]

    @staticmethod
    def _is_land_cover_feature(tags):
        landuse = tags.get("landuse")
        natural = tags.get("natural")

        if landuse:
            return True
        elif natural in NATURAL_GROUND_TYPES:
            return True

        return False

    def _render_feature(self, artist, feature):
        tags, points = feature

        self._set_artist_params(artist, tags)

        artist.draw_path(points)

    def _set_artist_params(self, artist, tags):
        landuse = tags.get("landuse")
        natural = tags.get("natural")
        terrain_type = landuse or natural

        style = self.config.get("style", {})
        terrain_style = style.get(terrain_type, {})

        stroke_color = terrain_style.get("stroke_color")
        stroke_width = terrain_style.get("stroke_width") or (1 if stroke_color else 0)
        fill_color = terrain_style.get("fill_color")

        artist.set_pen(stroke_color, stroke_width)
        artist.set_brush(fill_color)
