from .features import Features


class Roads(Features):
    def _get_features(self):
        relations = self.osm.query(
            "relations", ["highway", "railway", "aerialway", "route"]
        )
        ways = self.osm.query("ways", ["highway", "railway", "aerialway", "route"])

        return [
            *relations,
            *ways,
        ]

    def _get_feature_type(self, tags):
        feature_type = (
            tags.get("highway")
            or tags.get("railway")
            or tags.get("aerialway")
            or tags.get("route")
        )

        if tags.get("tunnel") == "yes":
            return [feature_type, f"{feature_type}-tunnel"]

        return feature_type
