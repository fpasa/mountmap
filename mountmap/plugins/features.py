from ..render_plugin import RenderLevel, RenderPlugin

from shapely.geometry import LineString, Polygon, Point


class Features(RenderPlugin):
    level = RenderLevel.FEATURES

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.borders = {}

    @property
    def style(self):
        return self.config.get("style", {})

    def render_artist(self, artist):
        self.borders = {}

        features = self._get_features()

        for feature in features:
            self._render_feature(artist, feature)

        self._render_borders(artist)

    def _get_features(self):
        raise NotImplementedError("._get_features() not implemented")

    def _render_feature(self, artist, feature):
        if feature["type"] == "relation":
            for way in feature["ways"]:
                self._render_way(artist, way, feature["tags"])
        elif feature["type"] == "way":
            self._render_way(artist, feature)
        elif feature["type"] == "node":
            self._render_node(artist, feature)

    def _render_way(self, artist, way, tags=None):
        if tags is None:
            tags = way["tags"]

        feature_type = self._get_feature_type(tags)

        # Ways can be dashed (railways)
        stroke_color, stroke_width, _ = self._get_feature_style_attrs(feature_type)
        _, dash, stroke_color2 = self._get_way_extra_style(feature_type)

        points = self._convert_points(way["nodes"])
        if dash:
            line1, line2 = self._split_line(points, dash)

            self._set_artist_params(artist, feature_type, 1)
            artist.draw_path(line1)

            if stroke_color2:
                artist.set_pen(stroke_color2, stroke_width + 1)
                artist.draw_path(line2)
        else:
            self._set_artist_params(artist, feature_type)
            artist.draw_path([points])

        # Ways might have an extra border, these are not rendered immediately, but
        # rather they are added to a pool, which then is used to draw borders,
        # without intersection
        self._add_border_for_rendering(points, feature_type)

    def _render_node(self, artist, node):
        pass

    def _convert_points(self, points):
        return [self.converter.convert(p) for p in points]

    def _set_artist_params(self, artist, feature_type, width_variation=0):
        stroke_color, stroke_width, fill_color = self._get_feature_style_attrs(
            feature_type
        )

        artist.set_pen(stroke_color, stroke_width + width_variation)
        artist.set_brush(fill_color)

    def _get_feature_type(self, tags):
        raise NotImplementedError("._get_feature_type() not implemented")

    def _get_feature_style(self, feature_type):
        if isinstance(feature_type, (list, tuple)):
            styles = [self.style.get(ft, {}) for ft in feature_type]
            feature_style = {}
            for style in styles:
                feature_style.update(style)
        else:
            feature_style = self.style.get(feature_type, {})

        return feature_style

    def _get_feature_style_attrs(self, feature_type):
        feature_style = self._get_feature_style(feature_type)

        stroke_color = feature_style.get("stroke_color")
        stroke_width = feature_style.get("stroke_width") or (1 if stroke_color else 0)
        fill_color = feature_style.get("fill_color")

        return stroke_color, stroke_width, fill_color

    def _add_border_for_rendering(self, points, feature_type):
        _, stroke_width, _ = self._get_feature_style_attrs(feature_type)
        border_color, _, _ = self._get_way_extra_style(feature_type)

        if border_color and stroke_width:
            line = LineString(points)
            self._add_border(border_color, line.buffer(stroke_width))

    def _add_border(self, border_color, area):
        if border_color in self.borders:
            self.borders[border_color] = self.borders[border_color].union(area)
        else:
            self.borders[border_color] = area

    def _render_borders(self, artist):
        for border_color, areas in self.borders.items():
            if isinstance(areas, Polygon):
                areas = [areas]

            for area in areas:
                border_points = list(area.exterior.coords)

                artist.set_pen(border_color, 1)
                artist.draw_path([border_points])

                for interior in area.interiors:
                    border_points = list(interior.coords)

                    artist.set_pen(border_color, 1)
                    artist.draw_path([border_points])

    def _get_way_extra_style(self, feature_type):
        feature_style = self._get_feature_style(feature_type)

        border_color = feature_style.get("border_color")
        dash = feature_style.get("dash")
        stroke_color2 = feature_style.get("stroke_color2")

        return border_color, dash, stroke_color2

    def _split_line(self, points, dash):
        line = LineString(points)
        line1 = []
        line2 = []

        for i in range(int(line.length / dash)):
            segment, line = self._cut(line, dash)
            points = list(segment.coords)

            if i % 2:
                line2.append(points)
            else:
                line1.append(points)

        return line1, line2

    @staticmethod
    def _cut(line, distance):
        # Cuts a line in two at a distance from its starting point
        if distance <= 0.0 or distance >= line.length:
            return LineString(line), None
        coords = list(line.coords)
        for i, p in enumerate(coords):
            pd = line.project(Point(p))
            if pd == distance:
                return LineString(coords[: i + 1]), LineString(coords[i:])
            if pd > distance:
                cp = line.interpolate(distance)
                return (
                    LineString(coords[:i] + [(cp.x, cp.y)]),
                    LineString([(cp.x, cp.y)] + coords[i:]),
                )
