import numpy as np

from ..render_plugin import RenderLevel, RenderPlugin


class Heatmap(RenderPlugin):
    level = RenderLevel.TERRAIN_SHADING

    def render_artist(self, artist):
        img_h, img_w = self.context.resolution

        elev = self.dem.get_elevation_grid(self.context.bbox, img_h, img_w)[0][::-1]
        pixel_values = (elev / elev.max() * 255).astype(np.uint8)

        artist.draw_bitmap((0, 0), pixel_values)
