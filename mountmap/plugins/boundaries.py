from .features import Features


class Boundaries(Features):
    def _get_features(self):
        relations = self.osm.query("relations", ["boundary"])
        ways = self.osm.query("ways", ["boundary"])

        return [
            *relations,
            *ways,
        ]

    def _get_feature_type(self, tags):
        return tags.get("boundary")
