import numpy as np
import skimage.transform
import skimage.filters

from ..interpolation import InterpolationType
from ..render_plugin import RenderLevel, RenderPlugin


class HillShading(RenderPlugin):
    level = RenderLevel.TERRAIN_SHADING

    @property
    def scale(self):
        return self.config.get("scale", 4)

    @property
    def blur(self):
        return self.config.get("blur", 2)

    @property
    def intensity(self):
        return self.config.get("intensity", 20)

    @property
    def illumination_vector(self):
        return self._norm_vector(
            self.config.get("illumination_vector", [1.0, -1.0, 1.0])
        )

    @property
    def interpolation_type(self):
        return {
            "nearest": InterpolationType.NEAREST,
            "linear": InterpolationType.LINEAR,
            "cubic": InterpolationType.CUBIC,
        }[self.config.get("interpolation", "cubic")]

    def render_artist(self, artist):
        img_h, img_w = self.context.resolution
        map_h = img_h / self.scale
        map_w = img_w / self.scale

        elev, xv, yv = self.dem.get_elevation_grid(
            self.context.bbox, map_h, map_w, interpolation=self.interpolation_type
        )

        illumination = self._compute_illumination_for_vector(elev, xv, yv)

        shadow = self._generate_shadow(illumination)
        if self.blur:
            shadow = skimage.filters.gaussian(
                shadow, sigma=self.blur, multichannel=True
            )

        artist.draw_bitmap((0, 0), shadow[::-1])

    def _compute_illumination_for_vector(self, elev, xv, yv, illumination_vec=None):
        if illumination_vec is None:
            illumination_vec = self.illumination_vector

        norm_vectors = self._compute_mean_norm_vectors(elev, xv, yv)
        illumination = np.dot(norm_vectors, illumination_vec)
        return illumination

    def _compute_mean_norm_vectors(self, data, xv, yv):
        return self._norm_vector(self._compute_norm_vectors(data, xv, yv).mean(axis=-1))

    def _compute_norm_vectors(self, data, xv, yv):
        """
        y
        ↑
        c     d
        *-----*
        +     +
        +     +
        *-----* -→ x
        a     b
        """

        ab_vectors = self._get_x_slope_vectors(data, xv, 0)
        ac_vectors = self._get_y_slope_vectors(data, yv, 0)
        norm_abc_vectors = self._norm_vector(np.cross(ab_vectors, ac_vectors))

        cd_vectors = self._get_x_slope_vectors(data, xv, 1)
        bd_vectors = self._get_y_slope_vectors(data, yv, 1)
        norm_bcd_vectors = self._norm_vector(np.cross(cd_vectors, bd_vectors))

        return np.stack([norm_abc_vectors, norm_bcd_vectors], axis=-1)

    def _get_x_slope_vectors(self, elev, xv, offset=0):
        h, w = elev.shape
        zero = np.zeros((h - 1, w - 1))

        starty = offset
        endy = h - 1 + offset

        return self._norm_vector(
            np.dstack(
                [
                    xv[starty:endy, 1:] - xv[starty:endy, :-1],
                    zero,
                    elev[starty:endy, 1:] - elev[starty:endy, :-1],
                ]
            )
        )

    def _get_y_slope_vectors(self, elev, yv, offset=0):
        h, w = elev.shape
        zero = np.zeros((h - 1, w - 1))

        startx = offset
        endx = w - 1 + offset

        return self._norm_vector(
            np.dstack(
                [
                    zero,
                    yv[1:, startx:endx] - yv[:-1, startx:endx],
                    elev[1:, startx:endx] - elev[:-1, startx:endx],
                ]
            )
        )

    @staticmethod
    def _norm_vector(vector):
        vector = np.array(vector)
        norm = np.linalg.norm(vector, ord=2, axis=-1)
        norm = norm.reshape(vector.shape[:-1] + (1,))
        return vector / norm

    def _generate_shadow(self, illumination):
        map_h, map_w = illumination.shape
        img_h, img_w = self.context.resolution

        shadow = np.zeros((map_h, map_w, 4))

        shadow[:, :, 3] = illumination * self.intensity + self.intensity

        return skimage.transform.resize(shadow, (img_h, img_w))
