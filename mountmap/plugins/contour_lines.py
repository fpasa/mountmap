from collections import namedtuple

import skimage.transform

from ..interpolation import InterpolationType
from ..render_plugin import RenderLevel, RenderPlugin

ContourConfig = namedtuple(
    "ContourConfig",
    # fields
    field_names=[
        "show_contours",
        "spacing",
        "skip_spacing",
        "stroke_width",
        "stroke_color",
        "show_labels",
        "label_spacing",
        "label_size",
    ],
)


class ContourLines(RenderPlugin):
    level = RenderLevel.TERRAIN_FEATURES

    @property
    def interpolation_type(self):
        return {
            "nearest": InterpolationType.NEAREST,
            "linear": InterpolationType.LINEAR,
            "cubic": InterpolationType.CUBIC,
        }[self.config.get("interpolation", "linear")]

    @property
    def major_conf(self):
        return self.config.get("major", {})

    @property
    def minor_conf(self):
        return self.config.get("minor", {})

    @property
    def major_spacing(self):
        return self.major_conf.get("spacing", 100)

    @property
    def minor_spacing(self):
        return self.minor_conf.get("spacing", 20)

    @property
    def major_stoke_width(self):
        return self.major_conf.get("stroke_width", 2)

    @property
    def minor_stoke_width(self):
        return self.minor_conf.get("stroke_width", 1)

    @property
    def major_stoke_color(self):
        return self.major_conf.get("stroke_color", "#555555aa")

    @property
    def minor_stoke_color(self):
        return self.minor_conf.get("stroke_color", "#55555577")

    @property
    def major_labels(self):
        return self.major_conf.get("labels", {})

    @property
    def minor_labels(self):
        return self.minor_conf.get("labels", False)

    @property
    def major_label_spacing(self):
        return self.major_labels.get("spacing", 250)

    @property
    def minor_label_spacing(self):
        return self.minor_labels.get("spacing", 250)

    @property
    def major_label_size(self):
        return self.major_labels.get("size", 10)

    @property
    def minor_label_size(self):
        return self.minor_labels.get("size", 8)

    def _get_contour_config(self, contour_type):
        if contour_type == "major":
            show_contours = self.major_conf is not False
            show_labels = show_contours and self.major_labels is not False

            return ContourConfig(
                show_contours=show_contours,
                spacing=show_contours and self.major_spacing,
                skip_spacing=None,
                stroke_width=show_contours and self.major_stoke_width,
                stroke_color=show_contours and self.major_stoke_color,
                show_labels=show_labels,
                label_spacing=show_labels and self.major_spacing,
                label_size=show_labels and self.major_label_size,
            )
        elif contour_type:
            show_contours = self.minor_conf is not False
            show_labels = show_contours and self.minor_labels is not False

            return ContourConfig(
                show_contours=show_contours,
                spacing=show_contours and self.minor_spacing,
                skip_spacing=show_contours and self.major_spacing,
                stroke_width=show_contours and self.minor_stoke_width,
                stroke_color=show_contours and self.minor_stoke_color,
                show_labels=show_labels,
                label_spacing=show_labels and self.minor_spacing,
                label_size=show_labels and self.minor_label_size,
            )

    def render_artist(self, artist):
        elev = self._get_elevation()

        self._render_contours(artist, elev, self._get_contour_config("major"))
        self._render_contours(artist, elev, self._get_contour_config("minor"))

    def _get_elevation(self):
        img_h, img_w = self.context.resolution

        return self.dem.get_elevation_grid(
            self.context.bbox, img_h, img_w, interpolation=self.interpolation_type
        )[0][::-1]

    def _render_contours(self, artist, elev, config):
        if config.show_contours and config.spacing:
            contours = self._find_contours(elev, config.spacing, config.skip_spacing)
            self._draw_contours(artist, contours, config)

    def _find_contours(self, elev, spacing, exclude_spacing=None):
        min_elev = elev.min()
        max_elev = elev.max()

        start_elev = int(min_elev // spacing * spacing)

        contours = []
        for contour_elev in range(start_elev, int(max_elev), spacing):
            if contour_elev == 0:
                # coastlines are drawn using vector data
                # TODO: this might create problem in areas under the sea level (skipping of
                #       one contour line)
                continue

            if exclude_spacing and not contour_elev % exclude_spacing:
                # skip minor if major is already there
                continue

            contours_at_elev = skimage.measure.find_contours(elev, contour_elev)
            flipped_contours_at_elev = self._flip_contours_coord_order(contours_at_elev)
            contours.append((elev, flipped_contours_at_elev))

        return contours

    @staticmethod
    def _flip_contours_coord_order(contours):
        return [contour[:, ::-1] for contour in contours]

    def _draw_contours(self, artist, contours, config):
        artist.set_pen(config.stroke_color, config.stroke_width)

        for contour in contours:
            artist.draw_path(contour[1])

        # if config.show_labels:
        #     chunked_contour = self._chunk_contours(artist, contours, config)
        # else:
        #     chunked_contour = contours
        #
        # self._draw_contour_chunks(artist, chunked_contour, config)

    # def _chunk_contours(self, artist, contours, config):
    #     artist.set_font(config.stroke_color, 'Freesans', config.label_size)
    #
    #     return (
    #         self._chunk_contour(
    #             contour,
    #             config.label_spacing,
    #             artist.text_size(config.label_size),
    #         )
    #         for contour in contours
    #     )
    #
    # def _chunk_contour(self, contour, spacing, text_size):
    #     chunks = []
    #
    #     # a contour at the same height might have different "parts"
    #     # we add some parts, splitting existing parts and return the list
    #     for part in contour:
    #         chunks.extend(self._split_contour_part(part, spacing, text_size))
    #
    #     return chunks
    #
    # def _split_contour_part(self, part, spacing, text_size):
    #     chunks = []
    #
    #     # distances one point to the next
    #     distances = (part[1:] - part[:-1]).sum(axis=1)
    #
    #     acc_dist = 0
    #     for i, distance in enumerate(distances):
    #         acc_dist += distance
    #
    #         if acc_dist > spacing:
    #             chunk = part[:i + 1]
    #
    #             if len(chunk):
    #                 chunks.append(chunk)
    #                 part = part[i + 1:]
    #                 acc_dist = 0
    #
    #     if len(part):
    #         chunks.append(part)
    #
    #     return chunks

    # def _draw_contour_chunks(self, artist, contours, config):
    #     artist.set_pen(config.stroke_color, config.stroke_width)
    #
    #     for contour in contours:
    #         print(contour)
    #         print(contour.shape)
    #         artist.draw_path(contour)
