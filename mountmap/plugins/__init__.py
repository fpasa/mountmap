from .background import Background
from .heatmap import Heatmap
from .contour_lines import ContourLines
from .hill_shading import HillShading
from .features import Features
from .land_cover import LandCover
from .water import Water
from .roads import Roads
from .buildings import Buildings
from .boundaries import Boundaries
from .labels import Labels
from .tracks import Tracks

plugins = [
    Background,
    Heatmap,
    ContourLines,
    HillShading,
    Features,
    LandCover,
    Water,
    Roads,
    Buildings,
    Boundaries,
    Tracks,
    Labels,
]
