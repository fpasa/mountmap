import shapely.affinity
from shapely.geometry import Polygon

from .features import Features


class Buildings(Features):
    @property
    def exaggerate(self):
        return self.config.get("exaggerate", 1)

    def _get_features(self):
        relations = self.osm.query("relations", ["building"])
        ways = self.osm.query("ways", ["building"])

        if self.exaggerate != 1:
            relations = self._exaggerate_relations(relations)
            ways = self._exaggerate_ways(ways)

        return [
            *relations,
            *ways,
        ]

    def _get_feature_type(self, tags):
        return tags.get("building")

    def _exaggerate_relations(self, relations):
        return (
            self._exaggerate_way(way)
            for relation in relations
            for way in relation["ways"]
        )

    def _exaggerate_ways(self, ways):
        return (self._exaggerate_way(way) for way in ways)

    def _exaggerate_way(self, way):
        points = ((float(n["lon"]), float(n["lat"])) for n in way["nodes"])

        polygon = Polygon(points)
        exaggerated_polygon = shapely.affinity.scale(
            polygon,
            xfact=self.exaggerate,
            yfact=self.exaggerate,
            origin=polygon.centroid,
        )

        way["nodes"] = list(exaggerated_polygon.exterior.coords)

        return way
