from ..render_plugin import RenderLevel, RenderPlugin


class Background(RenderPlugin):
    level = RenderLevel.BACKGROUND

    @property
    def fill_color(self):
        return self.config.get("fill_color")

    def render_artist(self, artist):
        if self.fill_color:
            artist.fill(self.fill_color)
