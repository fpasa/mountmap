import os
import math
import enum
import abc
import random
from collections import namedtuple

import numpy as np
from pyeasyga import pyeasyga
from shapely.geometry import box

from ..render_plugin import RenderLevel, RenderPlugin
from ..draw import Artist, Image

OPEN_SANS_SEMIBOLD = os.path.join(
    os.path.dirname(__file__), "resources", "OpenSans-SemiBold.ttf"
)

Label = namedtuple("Label", "text font size color xy measure")
PlacedLabel = namedtuple("PlacedLabel", "label anchor")

global_artist = Artist(Image(1, 1))


class Anchor(enum.IntEnum):
    S = 1
    SE = 2
    E = 3
    NE = 4
    N = 5
    NW = 6
    W = 7
    SW = 8


def anchor_label(xy, size, anchor, margin=5):
    sqrt_margin = math.sqrt(margin)

    if anchor == Anchor.S:
        return (
            xy[0] - size[0] / 2,
            xy[1] + margin,
        )
    elif anchor == Anchor.N:
        return (
            xy[0] - size[0] / 2,
            xy[1] - size[1] - margin,
        )
    elif anchor == Anchor.E:
        return (
            xy[0] + margin,
            xy[1] - size[1] / 2,
        )
    elif anchor == Anchor.W:
        return (
            xy[0] - size[0] - margin,
            xy[1] - size[1] / 2,
        )
    elif anchor == Anchor.SE:
        return (
            xy[0] + sqrt_margin,
            xy[1] + sqrt_margin,
        )
    elif anchor == Anchor.NE:
        return (
            xy[0] + sqrt_margin,
            xy[1] - size[1] - sqrt_margin,
        )
    elif anchor == Anchor.SW:
        return (
            xy[0] - size[0] - sqrt_margin,
            xy[1] + sqrt_margin,
        )
    elif anchor == Anchor.NW:
        return (
            xy[0] - size[0] - sqrt_margin,
            xy[1] - size[1] - sqrt_margin,
        )

    raise ValueError(f"invalid anchor {anchor}")


def label_box(label, anchor):
    global_artist.set_font(label.font, label.color, label.size)
    size = global_artist.text_size(label.text)

    xy = anchor_label(label.xy, size, anchor)
    return box(*xy, xy[0] + size[0], xy[1] + size[1])


class BoxSeries:
    def __init__(self, boxes=None):
        self.boxes = boxes or []

    def __iter__(self):
        return iter(self.boxes)

    def intersects(self, other_box):
        for b in self.boxes:
            if b is not other_box and b.intersects(other_box):
                return True

        return False

    def distance(self, other_box):
        distance = np.zeros((len(self.boxes) - 1,))
        i = 0
        for b in self.boxes:
            if b is not other_box:
                distance[i] = b.distance(other_box)
                i += 1

        return distance


class Labels(RenderPlugin):
    level = RenderLevel.LABELS

    SQUARE = np.zeros((4, 4, 3))

    @property
    def peaks(self):
        return self.osm.query("nodes", {"natural": "peak"})

    @property
    def strategy(self):
        return {
            "basic": BasicLabellingStrategy(),
            "genetic": GeneticLabellingStrategy(),
        }[self.config.get("strategy", "genetic")]

    def render_artist(self, artist):
        labels = self._get_labels()
        placed_labels = self._place_labels(labels)

        for label in placed_labels:
            self._render_label(artist, label)

    def _get_labels(self):
        return [*self._get_peak_labels()]

    def _get_peak_labels(self):
        font = OPEN_SANS_SEMIBOLD

        labels = []
        for peak in self.peaks:
            if "name" in peak:
                text = peak["name"]
                xy = self.converter.convert(peak)
                label = Label(text, font, None, "#000", xy, peak.get("ele", 0))
                labels.append(label)

            if "ele" in peak:
                text = peak["ele"]
                xy = self.converter.convert(peak)
                label = Label(text, font, 16, "#000", xy, text)
                labels.append(label)

        return labels

    def _place_labels(self, labels):
        return self.strategy.place(labels)

    def _render_label(self, artist, placed_label):
        label = placed_label.label
        artist.set_font(label.font, label.color, label.size)

        size = artist.text_size(label.text)
        xy = anchor_label(label.xy, size, placed_label.anchor)

        artist.draw_bitmap((label.xy[0] - 2, label.xy[1] - 2), self.SQUARE)
        artist.draw_text(xy, label.text)


def assign_unset_label_size(labels):
    unset_labels = [i for i, label in enumerate(labels) if not label.size]


class LabellingStrategy(abc.ABC):
    @abc.abstractmethod
    def place(self, labels):
        raise NotImplementedError(".place() not implemented")


class BasicLabellingStrategy(LabellingStrategy):
    """Just put all label below the point.
    This won't look very nice in most cases.
    """

    def place(self, labels):
        return [PlacedLabel(label, Anchor.S) for label in labels]


class GeneticLabellingStrategy(LabellingStrategy):
    """Use a simple genetic algorithm to optimize label placement."""

    def place(self, labels):
        ga = pyeasyga.GeneticAlgorithm(labels, generations=10)
        ga.create_individual = self._create_individual
        ga.mutate_function = self._mutate
        ga.fitness_function = self._fitness

        ga.run()

        fitness, anchors = ga.best_individual()

        return [
            PlacedLabel(label, Anchor(anchor)) for label, anchor in zip(labels, anchors)
        ]

    @staticmethod
    def _create_individual(labels):
        return [random.randint(1, 8) for _ in labels]

    @staticmethod
    def _mutate(individual):
        elem = random.randrange(len(individual))
        value = random.randrange(1, 9)
        individual[elem] = value

    @staticmethod
    def _fitness(individual, labels):
        fitness = 0

        boxes = BoxSeries(
            [label_box(label, anchor) for label, anchor in zip(labels, individual)]
        )
        for b, anchor in zip(boxes, individual):
            if anchor in (Anchor.S, Anchor.N):
                fitness += 8

            if not boxes.intersects(b):
                fitness += 10

            # fitness += boxes.distance(b).mean()

        return fitness
