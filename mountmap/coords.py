class CoordConverter:
    def __init__(self, bbox, resolution):
        self.bbox = bbox
        self.resolution = resolution

        self.lon_scale_factor = resolution[1] / bbox.width
        self.lat_scale_factor = resolution[0] / bbox.height

    def convert(self, points):
        if self._is_single_point(points):
            return self._convert_single(points)

        try:
            return [self._convert_single(point) for point in points]
        except TypeError:
            return self._convert_single(points)

    def convert_back(self, points):
        if self._is_single_point(points):
            return self._convert_back_single(points)

        try:
            return [self._convert_back_single(point) for point in points]
        except TypeError:
            return self._convert_back_single(points)

    def _convert_single(self, point):
        lon, lat = self._norm_point(point)
        return (
            (lon - self.bbox.start_lon) * self.lon_scale_factor,
            self.resolution[0] - (lat - self.bbox.start_lat) * self.lat_scale_factor,
        )

    def _convert_back_single(self, point):
        x, y = point
        return (
            self.bbox.start_lon + x / self.lon_scale_factor,
            self.bbox.start_lat + y / self.lat_scale_factor,
        )

    @staticmethod
    def _is_single_point(point):
        if isinstance(point, dict):
            return True
        elif isinstance(point, tuple) and len(point) == 2:
            return True

        return False

    @staticmethod
    def _norm_point(point):
        if isinstance(point, tuple):
            return point

        return float(point["lon"]), float(point["lat"])
