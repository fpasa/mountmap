import os
import sys
import time
import argparse

import imageio

from .config import YamlConfig, Config
from .render import render


def main():
    start = time.time()

    args = _parse_args()

    if args.config:
        with open(sys.argv[1]) as f:
            config = YamlConfig(f.read())
            config.load()
    else:
        config = Config()

    image = render(config)
    imageio.imsave(args.output, image)

    elapsed = time.time() - start
    print(f"Generated map in {elapsed:.2f} seconds")


def _parse_args():
    parser = argparse.ArgumentParser(
        prog="mountmap", description="Render maps from osm and elevation data.",
    )

    parser.add_argument(
        "config",
        type=_validate_file,
        help="configuration file in yaml format that specifies how to render the map",
    )

    parser.add_argument(
        "output", type=str, help="image file where the map will be saved",
    )

    return parser.parse_args()


def _validate_file(path):
    if not os.path.exists(path):
        raise argparse.ArgumentError(f'error: file "{path}" does not exist')

    return path


if __name__ == "__main__":
    main()
