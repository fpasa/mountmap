import abc
from collections import namedtuple

import yaml

from .bbox import BBox

PluginConf = namedtuple("PluginConf", "name options")


class Config(abc.ABC):
    def __init__(self):
        self.bbox = None
        self.resolution = None

        self.plugins = []

    @abc.abstractmethod
    def load(self):
        raise NotImplementedError(".load() not implemented")


class YamlConfig(Config):
    def __init__(self, yaml_str):
        super().__init__()

        self.yaml_str = yaml_str

    def load(self):
        config = yaml.safe_load(self.yaml_str)

        self._load_bbox(config)
        self._load_resolution(config)
        self._load_plugins(config)

    def _load_bbox(self, config):
        if "bbox" not in config:
            return

        start_lat, start_lon, end_lat, end_lon = config["bbox"]

        # Normalize if given order is wrong
        if end_lon < start_lon:
            temp = end_lon
            end_lon = start_lon
            start_lon = temp

        if end_lat < start_lat:
            temp = end_lat
            end_lat = start_lat
            start_lat = temp

        self.bbox = BBox(
            start_lon, start_lat, end_lon - start_lon, end_lat - start_lat,
        )

    def _load_resolution(self, config):
        self.resolution = config.get("size")

    def _load_plugins(self, config):
        plugins = config.get("plugins", [])
        self.plugins = [PluginConf(name, plugin) for name, plugin in plugins.items()]
