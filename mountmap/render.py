import os
import time

from .dem import DEM, JAXADEMProvider
from .osm import OSM, OverpassDataProvider
from .draw import Image
from .geo import relative_length_of_lon
from .render_plugin import RenderContext


def render(config):
    img = create_image(config)
    context = RenderContext(config.bbox, img.array.shape[:2])
    plugins = get_ordered_plugins(context, config)

    print(
        f"Rendering map with resolution of {img.array.shape[1]}x{img.array.shape[0]}:"
    )
    for plugin in plugins:
        start = time.time()
        print(f" - plugin {get_plugin_name(plugin.__class__)}", end="")

        plugin.render(img)

        elapsed = time.time() - start
        print(f" [{elapsed:.2f}s]")

    return img.array


def create_image(config):
    aspect_ratio_resolution = compute_aspect_ratio_resolution(
        config.bbox, config.resolution
    )
    return Image(*aspect_ratio_resolution)


def compute_aspect_ratio_resolution(bbox, resolution):
    res_height, res_width = resolution
    res_ratio = res_width / res_height

    rel_len_lon = relative_length_of_lon(bbox.center_lat)
    bbox_ratio = abs(bbox.width / bbox.height) * rel_len_lon

    if res_ratio > bbox_ratio:
        return res_height, int(res_height * bbox_ratio)
    elif bbox_ratio > res_ratio:
        return int(res_width / bbox_ratio), res_width

    return resolution


def get_ordered_plugins(context, config):
    dem = get_dem(config)
    osm = get_osm_data(config)

    return sorted(
        [
            instantiate_plugin(context, plugin_conf, dem, osm)
            for plugin_conf in config.plugins
        ],
        key=lambda plugin: plugin.level.value,
    )


def get_dem(config):
    provider = JAXADEMProvider()
    dem = DEM(config.bbox, provider)

    # Download data if needed
    dem._ensure_data_loaded()

    return dem


def get_osm_data(config):
    provider = OverpassDataProvider()
    return OSM(config.bbox, provider)


def load_plugins():
    from .plugins import plugins

    return {get_plugin_name(plugin): plugin for plugin in plugins}


def get_plugin_name(plugin):
    name = plugin.__name__
    return to_snake_case(name)


def to_snake_case(name):
    last_i = 0
    groups = []
    for i, l in enumerate(name[1:]):
        if l.isalpha() and l.upper() == l:
            groups.append(name[last_i : i + 1])
            last_i = i + 1

    groups.append(name[last_i:])

    return "_".join(map(str.lower, groups))


def instantiate_plugin(context, plugin_conf, dem, osm):
    plugin_map = load_plugins()

    if plugin_conf.name not in plugin_map:
        raise ValueError(f'Plugin "{plugin_conf.name}" was not found.')

    plugin_cls = plugin_map[plugin_conf.name]

    plugin = plugin_cls(context, plugin_conf.options, dem=dem, osm=osm)

    return plugin


# class FeaturesRenderer(OSMRenderer):
#     level = RenderLevel.FEATURES
#
#     def __init__(self, provider, color_map):
#         super().__init__(provider)
#         self.color_map = color_map
#
#     def render(self, map, bbox):
#         self._set_converter(bbox, map.shape)
#
#         relations = self.provider.query('relations', ['natural', 'waterway'])
#         ways = self.provider.query('ways', ['natural', 'waterway'])
#
#         for relation in relations:
#             if self._is_feature(relation):
#                 brush = self._get_brush(relation)
#                 map = self._render_realation(map, relation, brush)
#
#         for way in ways:
#             if self._is_feature(way):
#                 brush = self._get_brush(way)
#                 map = self._render_way(map, way, brush)
#
#         return map
#
#     def _is_feature(self, obj):
#         natural = obj['tags'].get('natural', None)
#
#         if natural not in NATURAL_GROUND_TYPES:
#             return True
#
#         return False
