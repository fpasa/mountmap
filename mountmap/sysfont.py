import fontconfig

system_fonts = fontconfig.query()
system_fonts = [system_fonts[i] for i in range(len(system_fonts))]
system_fonts = [
    (
        font.fullname and font.fullname[0][1] or None,
        font.family and font.family[0][1] or None,
        font.style and font.style[0][1] or None,
        font.file,
    )
    for font in system_fonts
    if "en" in font.get_languages()
]


def get_font(name):
    fonts = sorted(
        [
            (_score_font(name, font), font)
            for font in system_fonts
            if _filter_font(name, font)
        ],
        key=lambda v: v[0],
        reverse=True,
    )

    if not len(fonts):
        return None

    return fonts[0][1][3]


def _filter_font(name, font):
    fullname, family, _, _ = font

    if fullname and name.lower() in fullname.lower():
        return True

    if family and name.lower() in family.lower():
        return True

    return False


def _score_font(name, font):
    fullname, family, style, _ = font

    regular = (style.lower() == "regular") and 1 or 0

    if fullname and name.lower() == fullname.lower():
        return 40 + regular

    if fullname and name.lower() in fullname.lower():
        return 30 + regular

    if family and name.lower() == family.lower():
        return 20 + regular

    if family and name.lower() in family.lower():
        return 10 + regular

    return 0
