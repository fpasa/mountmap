import os

import numpy as np
from PIL import Image as PILImage
import aggdraw

from . import sysfont


class Image:
    def __init__(self, height, width):
        self.array = np.zeros((height, width, 3), dtype=np.uint8)


class Artist:
    def __init__(self, img):
        self.img = img

        self.pil_img = None
        self.canvas = None
        self._refresh_canvas()

        self.pen = None
        self.brush = None
        self.font = None

    def set_pen(self, color, width=1):
        if color is None:
            self.pen = None
            return

        color = self._transform_color(color)

        self.pen = aggdraw.Pen(color, width)

    def set_brush(self, color):
        if color is None:
            self.brush = None
            return

        color = self._transform_color(color)

        self.brush = aggdraw.Brush(color)

    def set_font(self, file, color, size=12):
        color = self._transform_color(color)

        if not os.path.exists(file):
            # Try to load system font
            new_file = sysfont.get_font(file)

            if not new_file:
                raise ValueError(f"font {file} cannot be found")

            file = new_file

        self.font = aggdraw.Font(color, file, size)

    def fill(self, color):
        self.render()

        color = self._transform_color(color)

        self.img.array[:, :] = color[:3]

        self._refresh_canvas()

    def draw_bitmap(self, xy, bitmap):
        self.render()

        x = int(xy[0])
        y = int(xy[1])
        normed_bitmap, alpha = self._norm_bitmap(bitmap)

        ih, iw = self.img.array.shape[:2]
        h, w = normed_bitmap.shape[:2]

        cl = int(abs(np.clip(x, -np.inf, 0)))
        ct = int(abs(np.clip(y, -np.inf, 0)))
        cr = int(w - np.clip(x + w - iw, 0, np.inf))
        cb = int(h - np.clip(y + h - ih, 0, np.inf))

        # do not draw if outside the image
        if cl >= cr or ct >= cb:
            return

        x = np.clip(x, 0, iw)
        y = np.clip(y, 0, ih)
        self.img.array[y : y + cb - ct, x : x + cr - cl] = (
            (1 - alpha[ct:cb, cl:cr]) * self.img.array[y : y + cb - ct, x : x + cr - cl]
            + alpha[ct:cb, cl:cr] * normed_bitmap[ct:cb, cl:cr]
        )

        self._refresh_canvas()

    def draw_path(self, points_lists):
        if self.pen is None and self.brush is None:
            return

        path = self._points_to_path(points_lists)
        self.canvas.path(path, self.pen, self.brush)

    def draw_text(self, xy, text):
        if not self.font:
            raise ValueError("font must be set before drawing text")

        self.canvas.text(xy, text, self.font)

    def text_size(self, text):
        if not self.font:
            raise ValueError("font must be set before drawing text")

        return self.canvas.textsize(text, self.font)

    def render(self):
        self.canvas.flush()
        self.img.array = np.array(self.pil_img)

    @staticmethod
    def _norm_bitmap(bitmap):
        w, h = bitmap.shape[:2]
        d = bitmap.shape[2] if len(bitmap.shape) == 3 else None

        if d is None or d == 1:
            rgb_bitmap = np.dstack([bitmap] * 3)
            alpha = np.ones((w, h, 1))
        elif d == 3:
            rgb_bitmap = bitmap
            alpha = np.ones((w, h, 1))
        elif d == 4:
            rgb_bitmap = bitmap[:, :, :3]
            alpha = (bitmap[:, :, 3] / 255.0).reshape((w, h, 1))
        else:
            raise ValueError(f"Inavlid bitmap shape {bitmap.shape}")

        return rgb_bitmap, alpha

    def _refresh_canvas(self):
        self.pil_img = PILImage.fromarray(self.img.array, mode="RGB")
        self.canvas = aggdraw.Draw(self.pil_img)

    @staticmethod
    def _points_to_path(points_lists):
        path = aggdraw.Path()

        for points in points_lists:
            path.moveto(*points[0])

            for point in points[1:]:
                path.lineto(*point)

        return path

    @staticmethod
    def _transform_color(color):
        """Splits hex RGB or RGBA color to color and opacity."""

        if isinstance(color, str):
            if color[0] != "#":
                raise ValueError(f"invalid color {color}")

            if len(color) == 4:
                return tuple(int(color[1 + i] * 2, 16) for i in range(3))
            if len(color) in (7, 9):
                return tuple(
                    int(color[1 + i * 2 : 1 + i * 2 + 2], 16)
                    for i in range((len(color) - 1) // 2)
                )
            else:
                raise ValueError(f"invalid color {color}")
        elif isinstance(color, tuple):
            if len(color) == 3:
                return (*color, 255)
            elif len(color) == 4:
                return color
            else:
                raise ValueError(f"invalid color {color}")
        else:
            raise ValueError(f"color must be string, but {color} given")
