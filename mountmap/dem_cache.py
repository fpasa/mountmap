import os
import pathlib

import tifffile
import imageio


class DEMCache:
    def __init__(self, cache_folder=None):
        if cache_folder is None:
            cache_folder = os.path.expanduser("~/.cache/mountmap/dem")

        self.cache_folder = pathlib.Path(cache_folder)
        os.makedirs(self.cache_folder, exist_ok=True)

    def get(self, filename, file_load_func):
        cache_path = self.cache_folder / filename

        if not os.path.exists(cache_path):
            dem_image = file_load_func()
            tifffile.imwrite(cache_path, dem_image, compress=9)
            return dem_image

        return imageio.imread(cache_path)
