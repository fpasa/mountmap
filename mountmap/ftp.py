import io
import ftplib

MB = 1024 * 1024


class FTP:
    def __init__(self, host, user, password):
        self.ftp = ftplib.FTP(host, user, password)
        self.ftp.voidcmd("TYPE I")

    def download(self, path, callback=lambda size, total, percent: None):
        path = str(path)

        size = self.ftp.size(path)
        downloaded = 0
        buffer = io.BytesIO()

        def download_chunk(data):
            nonlocal downloaded, buffer
            buffer.write(data)
            downloaded += len(data)
            callback(downloaded / MB, size / MB, downloaded / size)

        self.ftp.retrbinary(f"RETR {path}", download_chunk)

        buffer.seek(0)
        return buffer
