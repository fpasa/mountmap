import numpy as np


class BBox:
    def __init__(self, lon, lat, width, height):
        self.bbox = (lon, lat, width, height)

    @property
    def start_lon(self):
        return self.bbox[0]

    @property
    def start_lat(self):
        return self.bbox[1]

    @property
    def end_lon(self):
        return self.bbox[0] + self.bbox[2]

    @property
    def end_lat(self):
        return self.bbox[1] + self.bbox[3]

    @property
    def center_lon(self):
        return self.bbox[0] + self.bbox[2] / 2

    @property
    def center_lat(self):
        return self.bbox[1] + self.bbox[3] / 2

    @property
    def width(self):
        return self.bbox[2]

    @property
    def height(self):
        return self.bbox[3]

    @property
    def points(self):
        return (
            (self.start_lon, self.start_lat),
            (self.start_lon, self.end_lat),
            (self.end_lon, self.end_lat),
            (self.end_lon, self.start_lat),
        )

    def is_inside(self, lon, lat, tolerance=0):
        if np.any(lon < self.start_lon - tolerance) or np.any(
            lon > self.end_lon + tolerance
        ):
            return False
        elif np.any(lat < self.start_lat - tolerance) or np.any(
            lat > self.end_lat + tolerance
        ):
            return False

        return True

    def is_bbox_inside(self, other):
        return all(self.is_inside(*p) for p in other.points)

    def __str__(self):
        return f"{self.start_lon}, {self.start_lat} -> {self.end_lon}, {self.end_lat}"

    def __repr__(self):
        return f"<BBox {self.start_lon}, {self.start_lat} -> {self.end_lon}, {self.end_lat}>"
