import abc
import os
import re
import itertools
import gzip
import tarfile
import math

import imageio
import numpy as np

from .bbox import BBox
from .provider_consumer import Provider, Consumer
from .interpolation import get_interpolator_class, InterpolationType
from .dem_cache import DEMCache
from .ftp import FTP


class BBoxDEMProvider(Provider, abc.ABC):
    def __init__(self):
        super().__init__()

        self.data_bbox = BBox(0, 0, 0, 0)

    @abc.abstractmethod
    def _load_full_data(self, bbox):
        raise NotImplementedError("._load_full_data() not implemented")

    def load(self, bbox):
        full_data = self._load_full_data(bbox)
        indices, actual_bbox = self._compute_bbox_indices(bbox, full_data.shape)
        data = full_data[indices]
        return data, actual_bbox

    def _compute_bbox_indices(self, bbox, data_shape):
        data_h, data_w = data_shape

        start_x = (
            (bbox.start_lon - self.data_bbox.start_lon) / self.data_bbox.width * data_w
        )
        start_y = (
            (bbox.start_lat - self.data_bbox.start_lat) / self.data_bbox.height * data_h
        )

        width = bbox.width / self.data_bbox.width * data_w
        height = bbox.height / self.data_bbox.height * data_h

        end_x = int(math.ceil(start_x + width))
        end_y = int(math.ceil(start_y + height))

        start_x = int(start_x)
        start_y = int(start_y)

        return (
            self._create_indices(start_x, end_x, start_y, end_y),
            self._compute_actual_bbox(start_x, end_x, start_y, end_y, data_shape),
        )

    @staticmethod
    def _create_indices(start_x, end_x, start_y, end_y):
        slice_x = slice(start_x, end_x, 1)
        slice_y = slice(start_y, end_y, 1)
        indices = (slice_y, slice_x)
        return indices

    def _compute_actual_bbox(self, start_x, end_x, start_y, end_y, data_shape):
        data_h, data_w = data_shape

        return BBox(
            self.data_bbox.start_lon + start_x / data_w * self.data_bbox.width,
            self.data_bbox.start_lat + start_y / data_h * self.data_bbox.height,
            (end_x - start_x) / data_w * self.data_bbox.width,
            (end_y - start_y) / data_h * self.data_bbox.height,
        )


class FileDEMProvider(BBoxDEMProvider):
    def __init__(self, provider_file):
        super().__init__()

        if not os.path.exists(provider_file):
            raise FileNotFoundError(f'"{provider_file}" does not exist')

        self.provider_file = provider_file
        self.data_bbox = self.get_file_bbox()

    def get_file_bbox(self):
        basename = os.path.basename(self.provider_file)
        match = re.match(r"^([NS]\d+)([EW]\d+).*$", basename)

        if match is None:
            raise ValueError(
                f'file "{basename}" must have a filename starting with the coordinates (like N047E011)'
            )

        return BBox(self.parse_lon(match[2]), self.parse_lat(match[1]), 1, 1,)

    @staticmethod
    def parse_lat(lat):
        if lat[0] == "N":
            return int(lat[1:])
        elif lat[0] == "S":
            return -int(lat[1:])

        raise ValueError(f"invalid latitude {lat}")

    @staticmethod
    def parse_lon(lon):
        if lon[0] == "E":
            return int(lon[1:])
        elif lon[0] == "W":
            return -int(lon[1:])

        raise ValueError(f"invalid longitude {lon}")

    def _load_full_data(self, bbox):
        return imageio.imread(self.provider_file)[::-1]


class JAXADEMProvider(BBoxDEMProvider):
    """Loads data from the ALOS dataset from the Japanese Aerospace Exploratory Agency,
    available here: https://www.eorc.jaxa.jp/ALOS/en/aw3d30/
    """

    def __init__(self):
        super().__init__()

        self.ftp = None
        self.cache = DEMCache()

    def _load_full_data(self, bbox):
        self.data_bbox = self._get_data_bbox(bbox)
        return self._get_combined_dem(bbox)[::-1]

    def _get_combined_dem(self, bbox):
        dems = self._load_dems(bbox)
        vertical = self._are_tiles_vertical(bbox)
        return self._combine_dems(dems, vertical)

    def _load_dems(self, bbox):
        paths = self._get_ftp_tile_paths(bbox)

        dems = []
        for path in paths:
            filename = os.path.basename(path).replace(".tar.gz", ".tif")
            dem = self.cache.get(filename, lambda: self._download(path))
            dems.append(dem)

        return dems

    @staticmethod
    def _combine_dems(dems, vertical):
        if len(dems) == 1:
            return dems[0]

        if len(dems) == 4:
            return np.vstack(
                [np.hstack([dems[1], dems[3]]), np.hstack([dems[0], dems[2]])]
            )

        # Can only be 2 now
        if vertical:
            return np.vstack([dems[1], dems[0]])
        else:
            return np.hstack([dems[0], dems[1]])

    def _download(self, path):
        dem_file_stem = os.path.basename(path).replace(".tar.gz", "")
        archive_path = f"{dem_file_stem}/{dem_file_stem}_AVE_DSM.tif"

        print(f"Getting DEM file {dem_file_stem} from JAXA...")

        self._ensure_ftp_connected()

        print("  -> downloading", end="\r")
        compressed_file = self.ftp.download(path, self._print_download_progress)

        print("\n  -> extracting")
        decompressed_file = gzip.GzipFile(fileobj=compressed_file)
        archive = tarfile.open(fileobj=decompressed_file)
        dem = archive.extractfile(archive_path)

        return imageio.imread(dem)

    def _ensure_ftp_connected(self):
        if not self.ftp:
            self.ftp = FTP("ftp.eorc.jaxa.jp", "anonymous", "anonymous@")

    @staticmethod
    def _print_download_progress(size, total, percent):
        print(
            f"  -> downloading {size:.1f} / {total:.1f} MB [{percent*100:.0f} %]",
            end="\r",
        )

    def _get_ftp_tile_paths(self, bbox):
        paths = []
        for lon, lat in self._get_tiles(bbox):
            file_name = self._compute_name(lon, lat)
            folder_name = self._compute_name(lon - lon % 5, lat - lat % 5)
            paths.append(
                f"pub/ALOS/ext1/AW3D30/release_v1903/{folder_name}/{file_name}.tar.gz"
            )

        return paths

    @staticmethod
    def _get_rounded_coords(bbox):
        return (
            int(bbox.start_lon - bbox.start_lon % 1),
            int(bbox.end_lon - bbox.end_lon % 1),
            int(bbox.start_lat - bbox.start_lat % 1),
            int(bbox.end_lat - bbox.end_lat % 1),
        )

    def _get_data_bbox(self, bbox):
        start_lon, end_lon, start_lat, end_lat = self._get_rounded_coords(bbox)

        return BBox(
            start_lon,
            start_lat,
            math.ceil(bbox.end_lon) - start_lon,
            math.ceil(bbox.end_lat) - start_lat,
        )

    def _get_tiles(self, bbox):
        start_lon, end_lon, start_lat, end_lat = self._get_rounded_coords(bbox)

        lons = {start_lon, end_lon}
        lats = {start_lat, end_lat}

        return itertools.product(lons, lats)

    @staticmethod
    def _compute_name(lon, lat):
        lon_prefix = "E" if lon >= 0 else "W"
        lat_prefix = "N" if lat >= 0 else "S"
        return f"{lat_prefix}{abs(int(lat)):0>3}{lon_prefix}{abs(int(lon)):0>3}"

    def _are_tiles_vertical(self, bbox):
        start_lon, end_lon, start_lat, end_lat = self._get_rounded_coords(bbox)

        lons = {int(start_lon), int(end_lon)}
        lats = {int(start_lat), int(end_lat)}

        if len(lons) > len(lats):
            return False
        elif len(lons) < len(lats):
            return True

        # only one tile or 4 tiles
        return None


class DEM(Consumer):
    def get_elevation(self, lon, lat, interpolation=InterpolationType.LINEAR):
        if not self.bbox.is_inside(lon, lat):
            raise ValueError(
                f"Cannot get elevation of at {lon}, {lat} because only data "
                f"for the bounding box {self.bbox} has been loaded."
            )

        self._ensure_data_loaded()

        interpolator = get_interpolator_class(interpolation)(self.data, self.bbox)
        return interpolator.get_interpolated_value(lon, lat)

    def get_elevation_grid(
        self, bbox, grid_height, grid_width, interpolation=InterpolationType.LINEAR,
    ):
        if not self.bbox.is_bbox_inside(bbox):
            raise ValueError(
                f"Cannot get elevation grid on {bbox} because only data "
                f"for the bounding box {self.bbox} has been loaded. There "
                f"are points of the grid falling outside this range."
            )

        self._ensure_data_loaded()

        x = np.linspace(
            bbox.start_lon, bbox.end_lon - bbox.width / grid_width, grid_width
        )
        y = np.linspace(
            bbox.start_lat, bbox.end_lat - bbox.height / grid_height, grid_height
        )
        xv, yv = np.meshgrid(x, y)

        return self.get_elevation(xv, yv, interpolation), xv, yv
