import datetime
from urllib.request import urlopen

import overpy

from .provider_consumer import Provider, Consumer
from .osm_cache import OSMCache

QUERY = """
    (
        node({bbox});
        way({bbox});
        relation({bbox});
    );
    out body;
    >;
    out skel qt;
"""


class OverpassDataProvider(Provider):
    url = "http://overpass-api.de/api/interpreter"

    def __init__(self):
        super().__init__()

        self.cache = OSMCache()

    def load(self, bbox):
        cache_path = self._get_cache_file_path(bbox)
        xml_data = self.cache.get(cache_path, lambda: self._load_from_api(bbox))
        api = overpy.Overpass()
        return api.parse_xml(xml_data), bbox

    def _load_from_api(self, bbox):
        query = QUERY.format(bbox=self._gen_bbox_string(bbox))
        with urlopen(self.url, query.encode("utf-8")) as response:
            return response.read().decode("utf-8")

    @staticmethod
    def _get_cache_file_path(bbox):
        date = datetime.datetime.now().strftime("%Y-%m-%d")
        return f"{bbox.start_lon}-{bbox.start_lat}_{bbox.end_lon}-{bbox.end_lat}_{date}.xml"

    @staticmethod
    def _gen_bbox_string(bbox):
        return f"{bbox.start_lat},{bbox.start_lon},{bbox.end_lat},{bbox.end_lon}"


class XMLDataProvider(Provider):
    def __init__(self, xml_path):
        super().__init__()

        self.xml_path = xml_path

    def load(self, bbox):
        api = overpy.Overpass()

        with open(self.xml_path) as f:
            return api.parse_xml(f.read()), bbox


class OSM(Consumer):
    def query(self, elem, selector=None):
        self._ensure_data_loaded()

        objs = self._objects_by_elem(elem)
        selected = self._select_objects(objs, selector)
        return self._transform_objects(selected)

    def _objects_by_elem(self, elem):
        if elem == "nodes":
            return self.data.nodes
        elif elem == "ways":
            return self.data.ways
        elif elem == "areas":
            return self.data.areas
        elif elem == "relations":
            return self.data.relations

    def _select_objects(self, objs, selector):
        if selector is None:
            return objs
        elif isinstance(selector, str):
            return self._select_objects_with_tags(objs, [selector])
        elif isinstance(selector, list):
            return self._select_objects_with_tags(objs, selector)
        elif isinstance(selector, dict):
            return self._select_objects_with_tag_values(objs, selector)
        else:
            raise ValueError(f'selector "{selector}" not understood')

    @staticmethod
    def _select_objects_with_tags(objs, tags):
        return (obj for obj in objs for tag in tags if tag in obj.tags)

    def _select_objects_with_tag_values(self, objs, tags):
        res = objs
        for tag, val in tags.items():
            res = self._select_objects_with_tag_value(objs, tag, val)

        return res

    @staticmethod
    def _select_objects_with_tag_value(objs, tag, val):
        return (obj for obj in objs if tag in obj.tags and obj.tags[tag] == val)

    def _transform_objects(self, objs, roles=None):
        if roles is None:
            # Infinite iterator returning None
            roles = (None for _ in iter(int, 1))

        return (self._transform_object(obj, role) for obj, role in zip(objs, roles))

    def _transform_object(self, obj, role=None):
        if isinstance(obj, (overpy.Node, overpy.RelationNode)):
            return self._transform_node(obj, role)
        elif isinstance(obj, (overpy.Way, overpy.RelationWay)):
            return self._transform_way(obj, role)
        elif isinstance(obj, (overpy.Area, overpy.RelationArea)):
            return self._transform_area(obj, role)
        elif isinstance(obj, (overpy.Relation, overpy.RelationRelation)):
            return self._transform_relation(obj, role)

        raise ValueError(f'unknown obj "{obj}" of type {type(obj)}')

    @staticmethod
    def _transform_node(node, role=None):
        return {
            "type": "node",
            "lon": node.lon,
            "lat": node.lat,
            **node.tags,
            "role": role,
        }

    def _transform_way(self, way, role=None):
        return {
            "type": "way",
            "nodes": self._transform_objects(way.nodes),
            "tags": way.tags,
            "role": role,
        }

    def _transform_area(self, area, role=None):
        raise NotImplementedError("_transform_area not yet implemented")
        return {
            "type": "area",
            "tags": area.tags,
            "role": role,
        }

    def _transform_relation(self, relation, role=None):
        nodes = list(
            zip(
                *[
                    (member.resolve(), member.role)
                    for member in relation.members
                    if isinstance(member, overpy.RelationNode)
                ]
            )
        )
        ways = list(
            zip(
                *[
                    (member.resolve(), member.role)
                    for member in relation.members
                    if isinstance(member, overpy.RelationWay)
                ]
            )
        )
        areas = list(
            zip(
                *[
                    (member.resolve(), member.role)
                    for member in relation.members
                    if isinstance(member, overpy.RelationArea)
                ]
            )
        )

        return {
            "type": "relation",
            "tags": relation.tags,
            "role": role,
            "nodes": self._transform_objects(*nodes) if nodes else [],
            "ways": self._transform_objects(*ways) if ways else [],
            "areas": self._transform_objects(*areas) if areas else [],
        }
