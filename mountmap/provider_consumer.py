import abc


class Provider(abc.ABC):
    @abc.abstractmethod
    def load(self, bbox):
        raise NotImplementedError("Provider.load() method not implemented")


class Consumer(abc.ABC):
    def __init__(self, bbox, provider):
        self.bbox = bbox
        self.provider = provider
        self.data = None

    def _ensure_data_loaded(self):
        if self.data is None:
            # Update bbox, because load might return a larger bbox (e.g. for rasters),
            # depending on the provider implementation and data type
            self.data, self.bbox = self.provider.load(self.bbox)
