from .draw import Image
from .coords import CoordConverter


class CoordImage(Image):
    def __init__(self, bbox, resolution):
        super().__init__(resolution)

        self.bbox = bbox
        self.converter = CoordConverter(bbox, resolution)

    def draw_path(self, points_lists):
        pixel_points_lists = [self.converter.convert(points) for points in points_lists]

        return super().draw_path(pixel_points_lists)
