import abc
import enum

import numpy as np
import scipy.interpolate

from .coords import CoordConverter


class InterpolationType(enum.Enum):
    NEAREST = 1
    LINEAR = 2
    CUBIC = 3


class Interpolator(abc.ABC):
    def __init__(self, data, bbox):
        self.data = data
        self.bbox = bbox
        self.converter = CoordConverter(bbox, self.data.shape)

    @abc.abstractmethod
    def get_interpolated_value(self, lon, lat):
        raise NotImplementedError("Interpolation method not yet implemented")


class NearestInterpolator(Interpolator):
    def __init__(self, data, bbox):
        super().__init__(data[::-1], bbox)

    def get_interpolated_value(self, lon, lat):
        norm_coords = self._normalize_lon_lat(lon, lat)
        id_lon, id_lat = self.converter.convert(norm_coords)

        arr_id_lon = np.array(id_lon).astype(np.int64)
        arr_id_lat = np.array(id_lat).astype(np.int64)

        return self.data[arr_id_lat, arr_id_lon]

    @staticmethod
    def _normalize_lon_lat(lon, lat):
        lon = np.array(lon)
        lat = np.array(lat)

        return lon, lat


class ScipyInterpolator(Interpolator):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        height, width = self.data.shape

        x = np.linspace(
            self.bbox.start_lon, self.bbox.end_lon - self.bbox.width / width, width
        )
        y = np.linspace(
            self.bbox.start_lat, self.bbox.end_lat - self.bbox.height / height, height
        )

        self.interpolation_func = scipy.interpolate.interp2d(
            x, y, self.data, kind=self.kind, copy=False
        )

    def get_interpolated_value(self, lon, lat):
        norm_lon, norm_lat = self._normalize_lon_lat(lon, lat)

        elev = self.interpolation_func(norm_lon, norm_lat)

        # scipy-interpolate works like a cartesian product,
        # for the 1d case we need to extract the diagonal
        if len(np.array(lon).shape) == 1:
            # Here we need a double argsort
            # argsort 1 -> find where elements ended up
            # argsort 2 -> find the index of the elements in the array
            lon_idx = np.argsort(np.argsort(norm_lon))
            lat_idx = np.argsort(np.argsort(norm_lat))
            elev = elev[lat_idx, lon_idx]

        return self._normalize_elev(elev)

    @staticmethod
    def _normalize_lon_lat(lon, lat):
        lon = np.array(lon)
        lat = np.array(lat)

        # scipy.interpolate accepts only 1d arrays
        if len(lon.shape) > 1:
            lon = lon[0, :]
            lat = lat[:, 0]

        return lon, lat

    @staticmethod
    def _normalize_elev(elev):
        # scalar
        if elev.shape == (1,):
            return elev[0]

        return elev


class LinearInterpolator(ScipyInterpolator):
    kind = "linear"


class CubicInterpolator(ScipyInterpolator):
    kind = "cubic"


def get_interpolator_class(interpolation=InterpolationType.LINEAR):
    if interpolation is InterpolationType.NEAREST:
        return NearestInterpolator
    elif interpolation is InterpolationType.LINEAR:
        return LinearInterpolator
    elif interpolation is InterpolationType.CUBIC:
        return CubicInterpolator

    raise ValueError(f"No interpolator class for" f"interpolation code {interpolation}")
