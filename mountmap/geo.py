import math

A = 6378137
B = 6356752.314245
E2 = (A * A - B * B) / (A * A)
RAD = math.pi / 180


def length_of_lon(lat):
    cos = math.cos(lat * RAD)
    sin = math.sin(lat * RAD)
    return RAD * A * cos / math.sqrt(1 - E2 * sin * sin)


def length_of_lat(lat):
    sin = math.sin(lat * RAD)
    return RAD * A * (1 - E2) / math.pow(1 - E2 * sin * sin, 3 / 2)


def relative_length_of_lon(lat):
    length_lon = length_of_lon(lat)
    length_lat = length_of_lat(lat)

    return length_lon / length_lat
