import abc
import enum
from collections import namedtuple

from .coords import CoordConverter
from .draw import Artist


class RenderLevel(enum.Enum):
    BACKGROUND = 1
    LAND_AREAS = 2
    TERRAIN_SHADING = 3
    TERRAIN_FEATURES = 4
    FEATURES = 5
    LABELS = 6


RenderContext = namedtuple("RenderContext", "bbox resolution")


class RenderPlugin(abc.ABC):
    # define level to specify at which level the plugin should be run
    level = None

    def __init__(self, context, plugin_config, dem=None, osm=None):
        self.context = context
        self.config = plugin_config or {}
        self.dem = dem
        self.osm = osm

        self.converter = CoordConverter(context.bbox, context.resolution)

    def render(self, img):
        artist = Artist(img)
        self.render_artist(artist)
        artist.render()

    @abc.abstractmethod
    def render_artist(self, artist):
        raise NotImplementedError("RenderPlugin.render() not implemented")
