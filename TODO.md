# TODO

- Refactor land cover plugin to inherit from the features plugin.
- Devise better way to draw in coordinate space, without manually convert in each plugin.
- Move plugin test to own files.
- Moving drawing logic to own library.
- Validation of configuration.
- Normalize OSM tag values.
- Unify feature plugins?
- Line borders: normalize colors (as they are used as dict keys).
