import os
import pytest
from unittest.mock import patch, MagicMock

from mountmap import bbox, osm, dem, config


@pytest.fixture(scope="session")
def fix_path():
    test_folder = os.path.normpath(os.path.join(os.path.dirname(__file__), "data",))

    def fix_path_intern(path):
        return os.path.join(test_folder, path)

    return fix_path_intern


@pytest.fixture(scope="session")
def bbox_rotwand():
    return bbox.BBox(lon=11.9036, lat=47.63624, width=0.0963, height=0.0685,)


@pytest.fixture(scope="session")
def bbox_rotwand_lake():
    return bbox.BBox(lon=11.95223, lat=47.6482, width=0.00715, height=0.00462,)


@pytest.fixture(scope="session")
def dem_provider(fix_path):
    return dem.FileDEMProvider(fix_path("N047E011_AVE_DSM_COMPRESSED.tif"))


@pytest.fixture(scope="session")
def dem_data(bbox_rotwand, dem_provider):
    return dem.DEM(bbox_rotwand, dem_provider)


@pytest.fixture(scope="session")
def osm_provider(fix_path, bbox_rotwand_lake):
    return osm.XMLDataProvider(fix_path("overpass.xml"))


@pytest.fixture(scope="session")
def osm_data(bbox_rotwand_lake, osm_provider):
    return osm.OSM(bbox_rotwand_lake, osm_provider)


@pytest.fixture
def color_map():
    return {
        "forest": {"fill_color": "#00aa00",},
        "wood": {"fill_color": "#00aa00",},
        "meadow": {"fill_color": "#00ff00",},
        "grassland": {"fill_color": "#00ff00",},
        "water": {"fill_color": "#00ffff",},
        "lake": {"fill_color": "#00ffff",},
        "stream": {"stroke_color": "#00ffff",},
        "scrub": {"fill_color": "#aaff00",},
        "track": {
            "stroke_color": "#7e6552",
            "stroke_width": 2,
            "border_color": "#ffffff",
        },
        "path": {"stroke_color": "#7e6552",},
        "yes": {"fill_color": "#c49eb8",},
    }


@pytest.fixture
def basic_conf(fix_path):
    with open(fix_path("conf/basic.yml")) as f:
        return config.YamlConfig(f.read())


@pytest.fixture
def plugin_conf():
    def intern(name, options=None):
        plugin_conf = MagicMock()
        plugin_conf.name = name
        plugin_conf.options = options
        return plugin_conf

    return intern
