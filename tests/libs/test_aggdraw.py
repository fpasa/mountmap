import aggdraw
import imageio
import numpy as np
from PIL import Image


def test_path_hole(fix_path):
    path = aggdraw.Path()
    path.moveto(10, 10)
    path.lineto(10, 60)
    path.lineto(60, 60)
    path.lineto(60, 10)
    path.lineto(10, 10)
    path.moveto(20, 20)
    path.lineto(40, 20)
    path.lineto(40, 40)
    path.lineto(20, 40)
    path.lineto(20, 20)

    brush = aggdraw.Brush("#ff0000")

    im = Image.new("RGBA", (100, 100), (0, 0, 0, 0))
    draw = aggdraw.Draw(im)

    draw.polygon(path, None, brush)
    draw.flush()

    image = np.asarray(im)
    ref_image = imageio.imread(fix_path("results/hole.png"))

    assert image.shape == ref_image.shape
    assert np.sum(image - ref_image) == 0


def test_path_hole_other_direction(fix_path):
    path = aggdraw.Path()
    path.moveto(10, 10)
    path.lineto(60, 10)
    path.lineto(60, 60)
    path.lineto(10, 60)
    path.lineto(10, 10)
    path.moveto(20, 20)
    path.lineto(20, 40)
    path.lineto(40, 40)
    path.lineto(40, 20)
    path.lineto(20, 20)

    brush = aggdraw.Brush("#ff0000")

    im = Image.new("RGBA", (100, 100), (0, 0, 0, 0))
    draw = aggdraw.Draw(im)

    draw.polygon(path, None, brush)
    draw.flush()

    image = np.asarray(im)
    ref_image = imageio.imread(fix_path("results/hole.png"))

    assert image.shape == ref_image.shape
    assert np.sum(image - ref_image) == 0


def test_path_hole_first(fix_path):
    path = aggdraw.Path()
    path.moveto(20, 20)
    path.lineto(40, 20)
    path.lineto(40, 40)
    path.lineto(20, 40)
    path.lineto(20, 20)
    path.moveto(10, 10)
    path.lineto(10, 60)
    path.lineto(60, 60)
    path.lineto(60, 10)
    path.lineto(10, 10)

    brush = aggdraw.Brush("#ff0000")

    im = Image.new("RGBA", (100, 100), (0, 0, 0, 0))
    draw = aggdraw.Draw(im)

    draw.polygon(path, None, brush)
    draw.flush()

    image = np.asarray(im)
    ref_image = imageio.imread(fix_path("results/hole.png"))

    assert image.shape == ref_image.shape
    assert np.sum(image - ref_image) == 0


def test_path_concave(fix_path):
    path = aggdraw.Path()
    path.moveto(10, 10)
    path.lineto(10, 60)
    path.lineto(20, 60)
    path.lineto(20, 20)
    path.lineto(50, 20)
    path.lineto(50, 60)
    path.lineto(60, 60)
    path.lineto(60, 10)

    brush = aggdraw.Brush("#000000")

    im = Image.new("RGBA", (100, 100), (0, 0, 0, 0))
    draw = aggdraw.Draw(im)

    draw.polygon(path, None, brush)
    draw.flush()

    image = np.asarray(im)
    ref_image = imageio.imread(fix_path("results/concave.png"))

    assert image.shape == ref_image.shape
    assert np.sum(image - ref_image) == 0
