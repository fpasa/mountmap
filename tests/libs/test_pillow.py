from PIL import ImageColor


def test_getrgb():
    assert ImageColor.getrgb("#fff") == (255, 255, 255)
