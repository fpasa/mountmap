import os
import stat

import numpy as np
from numpy.testing import assert_array_equal
import imageio

from mountmap.dem_cache import DEMCache


def test_dem_cache(tmpdir):
    image = np.random.rand(10, 10)

    cache = DEMCache(tmpdir)
    cache.get("image.tif", lambda: image)

    assert os.path.exists(tmpdir / "image.tif")
    assert_array_equal(imageio.imread(str(tmpdir / "image.tif")), image)


def test_dem_cache_compression(tmpdir):
    image = np.ones((10, 10))

    cache = DEMCache(tmpdir)
    cache.get("image.tif", lambda: image)

    assert os.stat(tmpdir / "image.tif")[stat.ST_SIZE] == 341
