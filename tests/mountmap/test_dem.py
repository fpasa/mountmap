import io
from unittest.mock import patch, MagicMock

import pytest
import numpy as np
from numpy.testing import assert_array_equal

from mountmap.bbox import BBox
from mountmap.dem import InterpolationType, JAXADEMProvider, DEM

ONES = np.ones((2, 2))


def get_file_contents(path):
    with open(path, "rb") as file:
        return file.read()


def test_compute_bbox_indices_indices(dem_provider, bbox_rotwand):
    (y, x), actual_bbox = dem_provider._compute_bbox_indices(bbox_rotwand, (3600, 3600))

    assert x.start == 3252
    assert x.stop == 3600
    assert y.start == 2290
    assert y.stop == 2538


def test_compute_bbox_indices_bbox(dem_provider, bbox_rotwand):
    _, actual_bbox = dem_provider._compute_bbox_indices(bbox_rotwand, (3600, 3600))

    assert np.isclose(actual_bbox.start_lon, 11.9033333333333333), actual_bbox.start_lon
    assert np.isclose(actual_bbox.start_lat, 47.6361111111111111), actual_bbox.start_lat
    assert np.isclose(actual_bbox.end_lon, 12), actual_bbox.end_lon
    assert np.isclose(actual_bbox.end_lat, 47.705), actual_bbox.end_lat


def test_file_dem_data_load_data(dem_provider, bbox_rotwand):
    data, actual_bbox = dem_provider.load(bbox_rotwand)

    assert data.shape == (248, 348)


def test_dem_data_get_elevation(dem_data):
    elev = dem_data.get_elevation(
        11.94141, 47.65788, interpolation=InterpolationType.NEAREST
    )
    assert elev == 1880


def test_dem_data_get_elevation_Error(dem_data):
    with pytest.raises(ValueError):
        dem_data.get_elevation(
            12.94141, 47.65788, interpolation=InterpolationType.NEAREST
        )


def test_dem_data_get_elevation_multiple(dem_data):
    lon = np.array([11.94141, 11.95758, 11.9350608])
    lat = np.array([47.65788, 47.65062, 47.6460971])

    elev = dem_data.get_elevation(lon, lat, interpolation=InterpolationType.NEAREST)
    assert list(elev) == [1880, 1463, 1740]


def test_dem_data_get_elevation_linear(dem_data):
    elev = dem_data.get_elevation(11.94141, 47.65788)
    assert int(elev) == 1873


def test_dem_data_get_elevation_linear_multiple(dem_data):
    lon = np.array([11.94141, 11.95758, 11.9350608])
    lat = np.array([47.65788, 47.65062, 47.6460971])

    elev = dem_data.get_elevation(lon, lat)
    assert list(elev.astype(int)) == [1873, 1464, 1738]


def test_jaxa_provider_download(fix_path):
    jaxa = JAXADEMProvider()

    file_content = get_file_contents(fix_path("N047E012.tar.gz"))
    ftp_mock = MagicMock(download=MagicMock(return_value=io.BytesIO(file_content)))
    with patch.object(jaxa, "ftp", ftp_mock):
        dem = jaxa._download(
            "pub/ALOS/ext1/AW3D30/release_v1903/N045E010/N047E012.tar.gz"
        )

        assert isinstance(dem, np.ndarray)
        assert dem.shape == (3600, 3600)


@pytest.mark.parametrize(
    "bbox, paths",
    [
        (
            BBox(lon=11.9036, lat=47.63624, width=0.0963, height=0.0685),
            ["pub/ALOS/ext1/AW3D30/release_v1903/N045E010/N047E011.tar.gz"],
        ),
        (
            BBox(lon=-11.9036, lat=-47.63624, width=-0.0963, height=-0.0685),
            ["pub/ALOS/ext1/AW3D30/release_v1903/S050W015/S048W012.tar.gz"],
        ),
        (
            BBox(lon=11.9036, lat=47.63624, width=0.1963, height=0.0685),
            [
                "pub/ALOS/ext1/AW3D30/release_v1903/N045E010/N047E011.tar.gz",
                "pub/ALOS/ext1/AW3D30/release_v1903/N045E010/N047E012.tar.gz",
            ],
        ),
        (
            BBox(lon=11.50, lat=44.95, width=0.1, height=0.1),
            [
                "pub/ALOS/ext1/AW3D30/release_v1903/N040E010/N044E011.tar.gz",
                "pub/ALOS/ext1/AW3D30/release_v1903/N045E010/N045E011.tar.gz",
            ],
        ),
        (
            BBox(lon=11.95, lat=44.95, width=0.1, height=0.1),
            [
                "pub/ALOS/ext1/AW3D30/release_v1903/N040E010/N044E011.tar.gz",
                "pub/ALOS/ext1/AW3D30/release_v1903/N045E010/N045E011.tar.gz",
                "pub/ALOS/ext1/AW3D30/release_v1903/N040E010/N044E012.tar.gz",
                "pub/ALOS/ext1/AW3D30/release_v1903/N045E010/N045E012.tar.gz",
            ],
        ),
    ],
)
def test_jaxa_provider_get_ftp_tile_paths(bbox, paths):
    jaxa = JAXADEMProvider()
    assert jaxa._get_ftp_tile_paths(bbox) == paths


@pytest.mark.parametrize(
    "bbox, vertical",
    [
        (BBox(lon=11.9036, lat=47.63624, width=0.0963, height=0.0685), None),
        (BBox(lon=11.9036, lat=47.63624, width=0.1963, height=0.0685), False),
        (BBox(lon=11.50, lat=44.95, width=0.1, height=0.1), True),
        (BBox(lon=11.95, lat=44.95, width=0.1, height=0.1), None),
    ],
)
def test_jaxa_provider_are_tiles_vertical(bbox, vertical):
    jaxa = JAXADEMProvider()
    assert jaxa._are_tiles_vertical(bbox) == vertical


@pytest.mark.parametrize(
    "dems, vertical, shape, layout",
    [
        ([ONES], None, (2, 2), [[1]]),
        ([ONES, ONES], False, (2, 4), [[1, 2]]),
        ([ONES, ONES], True, (4, 2), [[2], [1]]),
        ([ONES, ONES, ONES, ONES], None, (4, 4), [[2, 4], [1, 3]]),
    ],
)
def test_jaxa_provider_combine_dems(dems, vertical, shape, layout):
    dems = [dem * (1 + i) for i, dem in enumerate(dems)]

    jaxa = JAXADEMProvider()
    combined = jaxa._combine_dems(dems, vertical)

    assert combined.shape == shape
    assert_array_equal(combined[::2, ::2], layout)


def test_jaxa_provider_get_data_bbox():
    bbox = BBox(lon=11.95, lat=44.95, width=0.1, height=0.1,)

    jaxa = JAXADEMProvider()
    data_bbox = jaxa._get_data_bbox(bbox)

    assert str(data_bbox) == "11, 44 -> 13, 46"


def test_jaxa_provider_load_full_data():
    bbox = BBox(lon=11.95, lat=44.95, width=0.1, height=0.1,)

    jaxa = JAXADEMProvider()
    data = jaxa._load_full_data(bbox)

    assert data.shape == (7200, 7200)


def test_jaxa_provider_load():
    bbox = BBox(lon=11.95, lat=44.95, width=0.1, height=0.1,)

    jaxa = DEM(bbox, JAXADEMProvider())

    assert jaxa.get_elevation(12.01, 45.02) == pytest.approx(0, abs=0.001)
    assert jaxa.get_elevation(12.01, 44.97) == pytest.approx(-3)
    assert jaxa.get_elevation(11.98, 44.97) == pytest.approx(-1)
    assert jaxa.get_elevation(11.98, 45.02) == pytest.approx(-3)


@pytest.mark.parametrize(
    "bbox, coords, elev",
    [
        (
            BBox(
                lon=12.70062,
                lat=47.14087,
                width=0.06874999999999964,
                height=0.08680000000000376,
            ),
            (12.755, 47.156944),
            3506,
        ),
        (
            BBox(
                lon=9.02493,
                lat=42.20086,
                width=0.05184000000000033,
                height=0.05949000000000382,
            ),
            (9.05774, 42.21587),
            2590.538336000019,
        ),
    ],
)
def test_jaxa_provider_altitude(bbox, coords, elev):
    jaxa = DEM(bbox, JAXADEMProvider())
    assert jaxa.get_elevation(*coords) == pytest.approx(elev)
