import os

from mountmap.osm_cache import OSMCache


def get_file_content(path):
    with open(path) as file:
        return file.read()


def test_dem_cache(tmpdir, fix_path):
    reference_xml = get_file_content(fix_path("overpass.xml"))

    cache = OSMCache(tmpdir)
    cache.get("osm.xml", lambda: reference_xml)

    assert os.path.exists(tmpdir / "osm.xml")
    assert cache.get("osm.xml", lambda: None) == reference_xml
