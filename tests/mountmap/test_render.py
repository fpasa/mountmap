from unittest.mock import patch, MagicMock

import numpy as np
import imageio

from mountmap import render
from mountmap import render_plugin
from mountmap import plugins


def test_create_image(bbox_rotwand):
    config = MagicMock()
    config.bbox = bbox_rotwand
    config.resolution = (600, 800)

    image = render.create_image(config)

    assert image.array.shape == (600, 569, 3)


def test_to_snake_case():
    snake = render.to_snake_case("ThisIsAString")
    assert snake == "this_is_a_string"


@patch(
    "mountmap.render.get_dem", return_value=None
)  # otherwise this test tries to load the DEM
def test_load_plugins(bbox_rotwand, plugin_conf):
    config = MagicMock()
    config.plugins = [plugin_conf("heatmap")]

    context = render_plugin.RenderContext(bbox_rotwand, (400, 400))
    conf_plugins = render.get_ordered_plugins(context, config)

    assert len(conf_plugins) == 1
    assert isinstance(conf_plugins[0], plugins.Heatmap)


def test_land_cover_rendering(fix_path, bbox_rotwand_lake, color_map, plugin_conf):
    config = MagicMock()
    config.bbox = bbox_rotwand_lake
    config.resolution = (400, 400)
    config.plugins = [plugin_conf("land_cover", {"style": color_map})]

    image = render.render(config)

    ref_image = imageio.imread(fix_path("results/land_cover.png"))

    assert image.shape == ref_image.shape
    assert np.sum(image - ref_image) == 0


def test_water_rendering(fix_path, bbox_rotwand_lake, color_map, plugin_conf):
    config = MagicMock()
    config.bbox = bbox_rotwand_lake
    config.resolution = (400, 400)
    config.plugins = [plugin_conf("water", {"style": color_map})]

    image = render.render(config)

    # imageio.imsave("water.png", image)

    ref_image = imageio.imread(fix_path("results/water.png"))

    assert image.shape == ref_image.shape
    assert np.sum(image - ref_image) == 0


# def test_features_rendering(fix_path, bbox_rotwand_lake, osm_provider, color_map, plugin_conf):
#     config = MagicMock()
#     config.bbox = bbox_rotwand_lake
#     config.resolution = (400, 400)
#     config.plugins = [
#         plugin_conf('land_cover', {
#             'style': color_map
#         }),
#         plugin_conf('contour_lines'),
#     ]
#
#     image = render.render(config)
#
#     ref_image = imageio.imread(fix_path('results/features.png'))
#
#     imageio.imsave('features.png', image)
#
#     assert image.shape == ref_image.shape
#     assert np.sum(image - ref_image) == 0


def test_complete_rendering(fix_path, bbox_rotwand_lake, color_map, plugin_conf):
    config = MagicMock()
    config.bbox = bbox_rotwand_lake
    config.resolution = (400, 400)
    config.plugins = [
        plugin_conf("land_cover", {"style": color_map,}),
        plugin_conf("contour_lines"),
        plugin_conf("hill_shading"),
        plugin_conf("water", {"style": color_map,}),
        plugin_conf("roads", {"style": color_map,}),
    ]

    image = render.render(config)

    # imageio.imsave("complete.png", image)

    ref_image = imageio.imread(fix_path("results/complete.png"))

    assert image.shape == ref_image.shape
    assert np.sum(image - ref_image) == 0
