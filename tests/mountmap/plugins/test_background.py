from unittest.mock import MagicMock

import imageio
import numpy as np

from mountmap import render


def test_plugin_background(fix_path, bbox_rotwand, plugin_conf):
    config = MagicMock()
    config.bbox = bbox_rotwand
    config.resolution = (400, 400)
    config.plugins = [plugin_conf("background", {"fill_color": "#ffff00"})]

    image = render.render(config)

    ref_image = imageio.imread(fix_path("results/background.png"))

    assert image.shape == ref_image.shape
    assert np.sum(image - ref_image) == 0
