import pytest

from mountmap.plugins.labels import (
    GeneticLabellingStrategy,
    Label,
    OPEN_SANS_SEMIBOLD,
    Anchor,
)


@pytest.mark.parametrize(
    "labels, result",
    [
        (
            [
                Label("test", OPEN_SANS_SEMIBOLD, 20, "#000", (100, 50)),
                Label("another", OPEN_SANS_SEMIBOLD, 20, "#000", (105, 55)),
            ],
            [Anchor.N, Anchor.S],
        )
    ],
)
def test_genetic_labelling_strategy(labels, result):
    strategy = GeneticLabellingStrategy()
    placed_labels = strategy.place(labels)

    for i, label in enumerate(placed_labels):
        assert label.anchor == result[i]
