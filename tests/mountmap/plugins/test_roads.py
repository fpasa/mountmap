from unittest.mock import MagicMock

import imageio
from numpy.testing import assert_array_equal

from mountmap import render


def test_roads_rendering(fix_path, bbox_rotwand_lake, color_map, plugin_conf):
    config = MagicMock()
    config.bbox = bbox_rotwand_lake
    config.resolution = (400, 400)
    config.plugins = [plugin_conf("roads", {"style": color_map})]

    image = render.render(config)

    # imageio.imsave('roads.png', image)

    ref_image = imageio.imread(fix_path("results/roads.png"))

    assert image.shape == ref_image.shape
    assert_array_equal(image, ref_image)
