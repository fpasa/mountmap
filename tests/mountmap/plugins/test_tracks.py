from unittest.mock import MagicMock

import imageio
from numpy.testing import assert_array_equal

from mountmap import render


def test_tracks_rendering(fix_path, bbox_rotwand_lake, plugin_conf):
    config = MagicMock()
    config.bbox = bbox_rotwand_lake
    config.resolution = (400, 400)
    config.plugins = [
        plugin_conf(
            "tracks",
            [
                {
                    "points": [
                        (47.65234, 11.95361),
                        (47.65186, 11.95406),
                        (47.65146, 11.9547),
                        (47.65106, 11.9551),
                        (47.651, 11.95604),
                        (47.65085, 11.95665),
                        (47.6509, 11.95679),
                        (47.6507, 11.95729),
                        (47.65075, 11.9578),
                        (47.65081, 11.95786),
                        (47.65077, 11.95801),
                        (47.65006, 11.95788),
                        (47.64984, 11.95763),
                        (47.64958, 11.95727),
                        (47.64921, 11.9562),
                        (47.6492, 11.95573),
                        (47.64935, 11.95542),
                        (47.64949, 11.95512),
                    ],
                }
            ],
        )
    ]

    image = render.render(config)

    # imageio.imsave("tracks.png", image)

    ref_image = imageio.imread(fix_path("results/tracks.png"))

    assert image.shape == ref_image.shape
    assert_array_equal(image, ref_image)
