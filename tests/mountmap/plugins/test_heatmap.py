from unittest.mock import MagicMock

import imageio
import numpy as np

from mountmap import render


def test_plugin_heatmap(fix_path, bbox_rotwand, plugin_conf):
    config = MagicMock()
    config.bbox = bbox_rotwand
    config.resolution = (400, 400)
    config.plugins = [plugin_conf("heatmap")]

    image = render.render(config)

    ref_image = imageio.imread(fix_path("results/heatmap.png"))

    assert image.shape == ref_image.shape
    assert np.sum(image - ref_image) == 0
