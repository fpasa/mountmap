from unittest.mock import MagicMock

import imageio
import numpy as np
from numpy.testing import assert_array_equal
import pytest

from mountmap import render
from mountmap.render_plugin import RenderContext
from mountmap.plugins import HillShading


def test_compute_illumination(bbox_rotwand):
    context = RenderContext(bbox_rotwand, (400, 400))
    plugin = HillShading(context, None, None)

    elev = np.array([[1.0, 2.0], [1.5, 1.0]])
    xv = np.array([[1.0, 2.0], [1.0, 2.0]])
    yv = np.array([[1.0, 1.0], [2.0, 2.0]])

    illumination = plugin._compute_illumination_for_vector(elev, xv, yv)

    assert illumination.shape == (1, 1)
    assert illumination[0, 0] == pytest.approx(0.2721655269759087)


def test_plugin_hill_shading(fix_path, bbox_rotwand, plugin_conf):
    config = MagicMock()
    config.bbox = bbox_rotwand
    config.resolution = (400, 400)
    config.plugins = [
        plugin_conf("background", {"fill_color": "#ffffff"}),
        plugin_conf("hill_shading"),
    ]

    image = render.render(config)

    # imageio.imsave("hill_shading.png", image)

    ref_image = imageio.imread(fix_path("results/hill_shading.png"))

    assert image.shape == ref_image.shape
    assert_array_equal(image, ref_image)
