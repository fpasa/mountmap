from unittest.mock import MagicMock

import imageio
from numpy.testing import assert_array_equal

from mountmap import render


def test_buildings_rendering(fix_path, bbox_rotwand_lake, color_map, plugin_conf):
    config = MagicMock()
    config.bbox = bbox_rotwand_lake
    config.resolution = (400, 400)
    config.plugins = [plugin_conf("buildings", {"style": color_map,})]

    image = render.render(config)

    # imageio.imsave('buildings.png', image)

    ref_image = imageio.imread(fix_path("results/buildings.png"))

    assert image.shape == ref_image.shape
    assert_array_equal(image, ref_image)


def test_buildings_rendering_options(
    fix_path, bbox_rotwand_lake, color_map, plugin_conf
):
    config = MagicMock()
    config.bbox = bbox_rotwand_lake
    config.resolution = (400, 400)
    config.plugins = [plugin_conf("buildings", {"style": color_map, "exaggerate": 2,})]

    image = render.render(config)

    # imageio.imsave('buildings_options.png', image)

    ref_image = imageio.imread(fix_path("results/buildings_options.png"))

    assert image.shape == ref_image.shape
    assert_array_equal(image, ref_image)
