from unittest.mock import MagicMock

import imageio
import numpy as np

from mountmap import render


def test_plugin_contour_lines(fix_path, bbox_rotwand, plugin_conf):
    config = MagicMock()
    config.bbox = bbox_rotwand
    config.resolution = (400, 400)
    config.plugins = [plugin_conf("contour_lines")]

    image = render.render(config)

    # imageio.imsave('contour.png', image)

    ref_image = imageio.imread(fix_path("results/contour.png"))

    assert image.shape == ref_image.shape
    assert np.sum(image - ref_image) == 0


def test_plugin_contour_lines_options(fix_path, bbox_rotwand, plugin_conf):
    config = MagicMock()
    config.bbox = bbox_rotwand
    config.resolution = (400, 400)
    config.plugins = [
        plugin_conf(
            "contour_lines",
            {
                "major": {
                    "spacing": 50,
                    "stroke_color": "#ffff0099",
                    "stroke_width": 3,
                },
                "minor": False,
            },
        )
    ]

    image = render.render(config)

    ref_image = imageio.imread(fix_path("results/contour_options.png"))

    assert image.shape == ref_image.shape
    assert np.sum(image - ref_image) == 0
