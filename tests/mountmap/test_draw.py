import numpy as np
import pytest

from mountmap.bbox import BBox
from mountmap.draw import Image, Artist
from mountmap.draw_geo import CoordConverter


@pytest.fixture
def rect():
    return [[(10, 20), (10, 70), (60, 70), (60, 20),]]


def test_fill(rect):
    img = Image(100, 100)

    assert np.isclose(img.array.mean(), 0)

    artist = Artist(img)
    artist.fill("#fff")
    artist.render()

    assert np.isclose(img.array.mean(), 255)


def test_draw_bitmap():
    img = Image(100, 100)

    box = np.ones((20, 10))

    artist = Artist(img)
    artist.draw_bitmap((10, 10), box)
    artist.render()

    rect_mean = img.array[10:30, 10:20, :].mean()
    assert np.isclose(rect_mean, 1)

    img.array[10:30, 10:20, :] = 0
    assert np.isclose(img.array.mean(), 0)


def test_draw_bitmap_outside_bottom_right():
    img = Image(100, 100)

    box = np.ones((4, 4, 3))

    artist = Artist(img)
    artist.draw_bitmap((98, 98), box)
    artist.render()

    assert np.isclose(img.array.mean(), 4e-4)
    assert np.isclose(img.array[98:, 98:].mean(), 1)


def test_draw_bitmap_outside_top_left():
    img = Image(100, 100)

    box = np.ones((4, 4, 3))

    artist = Artist(img)
    artist.draw_bitmap((-2, -2), box)
    artist.render()

    assert np.isclose(img.array.mean(), 4e-4)
    assert np.isclose(img.array[:2, :2].mean(), 1)


def test_draw_path_brush(rect):
    img = Image(100, 100)

    artist = Artist(img)
    artist.set_pen("#fff", 0)
    artist.set_brush("#fff")
    artist.draw_path(rect)
    artist.render()

    rect_mean = img.array[20:70, 10:60, :].mean()
    assert np.isclose(rect_mean, 255)

    img.array[20:70, 10:60, :] = 0
    assert np.isclose(img.array.mean(), 0)


def test_draw_set_font(fix_path):
    img = Image(100, 100)
    artist = Artist(img)

    artist.set_font(fix_path("Roboto-Regular.ttf"), (0, 0, 0))
    artist.set_font("DejaVu", (0, 0, 0))


def test_draw_font(fix_path):
    img = Image(100, 100)
    artist = Artist(img)

    artist.fill("#fff")

    artist.set_font(fix_path("Roboto-Regular.ttf"), (0, 0, 0))
    artist.draw_text((10, 40), "Test 123")

    artist.render()

    rect_mean = img.array[:40, :, :].mean()
    assert np.isclose(rect_mean, 255)

    assert img.array.sum() == 7567608


def test_draw_text_size(fix_path):
    img = Image(100, 100)
    artist = Artist(img)

    artist.set_font(fix_path("Roboto-Regular.ttf"), (0, 0, 0))

    assert artist.text_size("Test 123") == pytest.approx((47, 14))


def test_coord_converter():
    converter = CoordConverter(BBox(1, 1, 1, 0.5), (500, 1000))
    res = converter.convert({"lon": 1.2, "lat": 1.1,})

    assert round(res[0]) == 200
    assert round(res[1]) == 400
