from mountmap.geo import length_of_lon, length_of_lat, relative_length_of_lon


def test_length_lon():
    assert int(round(length_of_lon(0))) == 111319
    assert int(round(length_of_lon(45))) == 78847


def test_length_lat():
    assert int(round(length_of_lat(0))) == 110574
    assert int(round(length_of_lat(45))) == 111132


def test_relative_length_of_lon():
    assert int(round(relative_length_of_lon(0) * 1000)) == 1007
    assert int(round(relative_length_of_lon(45) * 1000)) == 709
