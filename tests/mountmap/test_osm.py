def test_osm_data_query_nodes(osm_data):
    nodes = osm_data.query("nodes")
    assert len(list(nodes)) == 6069


def test_osm_data_query_nodes_tag(osm_data):
    nodes = osm_data.query("nodes", "amenity")
    assert len(list(nodes)) == 2


def test_osm_data_query_nodes_tag_value(osm_data):
    nodes = osm_data.query("nodes", {"material": "wood"})
    assert len(list(nodes)) == 1


def test_osm_data_query_ways(osm_data):
    osm_data.query("ways")


def test_osm_data_query_relations(osm_data):
    osm_data.query("relations")
